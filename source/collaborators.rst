
Collaborators
=============

- `Hartwig Anzt <https://www.yin.kit.edu/120_2230.php>`_, Karlsruher Institut für Technologie
- `Erin Carson <https://www.karlin.mff.cuni.cz/~carson/>`_, Charles University
- `Nick Higham <http://www.maths.manchester.ac.uk/~higham/>`_, University of Manchester
