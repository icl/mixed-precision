
Mixed-Precision Links
=====================

- `HPL-AI Benchmark <https://hpl-ai.bitbucket.io/hpl-ai>`_
- `Early press release <https://www.nextplatform.com/2019/06/17/doing-the-math-the-reality-of-hpc-and-ai-convergence/>`_
- `NVIDIA blog post <https://blogs.nvidia.com/blog/2019/06/17/hpc-ai-performance-record-summit/>`_
- `Nick Higham's blog post <https://nickhigham.wordpress.com/2018/12/03/half-precision-arithmetic-fp16-versus-bfloat16/>`_
