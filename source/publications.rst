
Publications
============

#. Azzam Haidar, Stanimire Tomov, Jack Dongarra, Nicholas J. Higham,
   *Harnessing GPU Tensor Cores for Fast FP16 Arithmetic to Speed up
   Mixed-Precision Iterative Refinement Solvers*, In Procedeedings of SC18, 2018
   [ `Online <https://dl.acm.org/citation.cfm?id=3291719>`__ ],
   [ `Preprint PDF <http://www.netlib.org/utk/people/JackDongarra/PAPERS/haidar_fp16_sc18.pdf>`__ ]
#. Erin Carson and Nicholas J. Higham,
   *Accelerating the Solution of Linear Systems by Iterative Refinement in Three Precisions*,
   SIAM J. SCI. COMPUT., Vol. 40, No. 2, pp. A817--A847, 2018,
   [ `PDF <https://epubs.siam.org/doi/pdf/10.1137/17M1140819>`__ ]
#. Erin Carson and Nicholas J. Higham, 
   *A New Analysis of Iterative Refinement and its Application to Accurate Solution of Ill-Conditioned Sparse Linear Systems*,
   2017,
   [ `MIMS Preprint <http://eprints.maths.manchester.ac.uk/2537/>`__ ]
#. Nicholas J. Higham, Srikara Pranesh, and Mawussi Zounon,
   *Squeezing a Matrix into Half Precision, with an Application to Solving Linear Systems*,
   2018,
   [ `MIMS Preprint <http://eprints.maths.manchester.ac.uk/2678/>`__ ]
#. Pierre Blanchard, Nicholas J. Higham, Florent Lopez, Theo Mary, and Srikara Pranesh,
   *Mixed Precision Block Fused Multiply-Add: Error Analysis and Application to GPU Tensor Cores*,
   2019,
   [ `MIMS Preprint <http://eprints.maths.manchester.ac.uk/2733/>`__ ]
